scalar FileUpload

enum UserRole {
  user
  admin
}

enum VolunteerAction {
  email
  externForm
  internForm
}

type Query {
  isLogged: User
  isAccountValidated(email: String!, token: String!): Boolean
  stages(
    entries: [[String]]
    postCode: String
    search: String
    isValidated: Boolean
    favoritesForUser: String
    stagesCollective: [[String]]
    limit: Int
    sort: String
    way: String
    canAdmin: Boolean
  ): [Stage!]!
  articles(
    search: String
    tag: String
    stageId: String
    limit: Int
    sort: String
    way: String
  ): [Article!]!
  users: [User!]!
  user(id: String): User!
  stagesCollective: [Stage!]!
  article(id: String): Article!
  stage(id: String, name: String): Stage!
  events(
    stageId: String
    startingDate: String
    entries: [[String]]
    search: String
    limit: Int
    sort: String
    way: String
    notFinished: Boolean
    favoritesForUser: String
    canAdmin: Boolean
  ): [Event!]!
  event(id: String): Event!
  search(
    searchEvent: Boolean
    searchStage: Boolean
    searchArticle: Boolean
    searchValue: String!
  ): Search

  collections: [Collection!]!
  categories: [Entry!]!
  category(id: String): Entry!
  entry(id: String): Entry!
  participants(eventId: String): [User]
  eventParticipants(eventId: String): [ParticipateEvent]
}

type Mutation {
  # Auth resolvers
  login(email: String!, password: String!, persistentConnection: Boolean!): User
  logout: Boolean
  validateAccount(email: String!, token: String!, password: String!): Boolean
  updateAccount(surname: String!, lastname: String!, email: String!): User
  updateAccountPassword(password: String!): Boolean
  deleteAccount: Boolean
  sendResetPasswordEmail(resetPasswordInfos: ResetPasswordInfos): Int
  validateActionCode(validateActionCodeInfos: ValidateActionCodeInfos): Boolean
  updateForgotPassword(email: String!, codeId: Int!, code: Int!, password: String!): Boolean

  # User resolvers
  addUser(
    surname: String!
    lastname: String!
    email: String!
    role: UserRole!
    stageIds: [ID!]!
  ): User
  editUser(
    userId: ID!
    surname: String!
    lastname: String!
    email: String!
    role: UserRole!
    stageIds: [ID!]!
  ): User
  sendValidationEmail(userId: ID!): Boolean
  deleteUser(userId: Int!): Boolean

  # Stage resolvers
  createStage(
    stageInfos: StageInfos
    description: String!
    mainPictures: [InputPictureType]
    pictures: [InputPictureType]
    partnerPictures: [InputPictureType]
  ): Stage

  editStage(
    stageId: Int!
    stageInfos: StageInfos
    description: String!
    mainPictures: [InputPictureType]
    pictures: [InputPictureType]
    partnerPictures: [InputPictureType]
  ): Stage

  deleteStage(stageId: Int!, deleteEvent: Boolean): Boolean

  addStageVolunteer(stageId: Int!, volunteerInfos: VolunteerInfos): Boolean

  # Event resolvers
  createEvent(
    eventInfos: EventInfos
    stageId: Int!
    description: String!
    practicalInfo: String
    mainPictures: [InputPictureType]
    pictures: [InputPictureType]
  ): Event

  editEvent(
    eventInfos: EventInfos
    eventId: Int!
    mainPictures: [InputPictureType]
    pictures: [InputPictureType]
    description: String!
    practicalInfo: String
  ): Event

  deleteEvent(eventId: Int!): Boolean

  # Article resolvers
  createArticle(
    articleInfos: ArticleInfos
    content: String!
    pictures: [InputPictureType]
    mainPictures: [InputPictureType]
  ): Article
  editArticle(
    articleInfos: ArticleInfos
    articleId: Int!
    content: String!
    mainPictures: [InputPictureType]
    pictures: [InputPictureType]
  ): Article
  deleteArticle(articleId: Int!): Boolean

  # Other resolvers
  addFavorite(
    stageId: Int
    eventId: Int
    userId: Int
    favorite: Boolean!
  ): Boolean

  createNewsletterEmail(email: String!): Newsletter
  createNewsletterUser(userId: Int!): Newsletter
  addEventParticipate(eventId: Int!, userId: Int!): Boolean
  removeEventParticipate(eventId: Int!, userId: Int!): Boolean

  addParticipateEvent(eventId: Int!, participateEventInfos: ParticipateEventInfos!): Boolean
  removeParticipateEvent(eventId: Int!, token: String!): Boolean

  sendContactFormEmail(contactForm: ContactFormInfos!): Boolean
  suggestEvent(suggestEventInfos: SuggestEventInfos!): Boolean
}

type User {
  id: ID!
  surname: String
  lastname: String
  email: String
  role: UserRole
  phone: String
  address: String
  postCode: String
  city: String
  isEmailValidated: Boolean
  validationEmailToken: String
  participatedAt: String
  stages: [Stage!]
}
type Stage {
  id: ID!
  name: String
  startedAt: String
  endedAt: String
  email: String
  phone: String
  address: String
  postCode: String
  city: String
  website: String
  description: String
  shortDescription: String
  createdAt: String
  updatedAt: String
  lat: String
  lng: String
  entries: [Entry]
  events: [Event]
  volunteers: [Volunteer]
  favorites: [User]
  referents: [User]
  contact_id: String
  logoPictures: [Picture]
  mainPictures: [Picture]
  pictures: [Picture]
  isValidated: Boolean
  dateValidation: String
  userValidated: User
  socialNetwork: String
  activity: String
  volunteerDescription: String
  openingHours: [OpeningHour]
  articles: [Article]
  extendStage: Boolean
  memberOf: [Stage]
  members: [Stage]
  referencingStage: Stage
  volunteerForm: String
  volunteerAction: VolunteerAction
}
type Picture {
  id: ID!
  label: String
  originalPicturePath: String
  originalPictureFilename: String
  position: Int
  logo: Boolean
  main: Boolean
  partner: Boolean
}
type Search {
  events: [Event]
  stages: [Stage]
  articles: [Article]
}
type Event {
  id: ID!
  label: String
  shortDescription: String
  facebookUrl: String
  description: String
  startedAt: String
  endedAt: String
  dateRule: String
  published: Boolean
  address: String
  postCode: String
  city: String
  lat: Float
  lng: Float
  entries: [Entry]
  stages: [Stage]
  participants: [User]
  referents: [User]
  favorites: [User]
  createdAt: String
  updatedAt: String
  pictures: [Picture]
  registerLink: String
  limitPlace: Int
  practicalInfo: String
  parentEvent: Event
  subEvents: [Event]
  nbParticipants: Int
  referencingStage: Stage
  category: String
  participateButton: Boolean
}
type Newsletter {
  id: ID!
  email: String
}
type Day {
  id: String
  day: String
  selected: Boolean
  identifier: String
}
type List {
  entry: [String]
}
type OpeningHour {
  id: ID!
  days: [Day]
  hours: [[String]]
  place: String
}
type Collection {
  id: ID!
  label: String
  code: String
  enabled: Boolean
  multipleSelection: Boolean
  withDescription: Boolean
  position: Int
  entries: [Entry]
  stage: Boolean
  event: Boolean
  filter: Boolean
}
type Entry {
  id: ID!
  code: String
  label: String
  icon: String
  color: String
  description: String
  subEntries: [Entry]
  collection: Collection
  parentEntry: Entry
  position: Int
  stageEntries: StageEntries
}
type StageEntries {
  id: ID!
  entry: Entry
  linkDescription: String
  topSEO: Boolean
}
type Article {
  id: ID!
  label: String
  shortDescription: String
  content: String
  createdAt: String
  updatedAt: String
  stages: [Stage]
  pictures: [Picture]
  bannerPrincipalPicture: Boolean
}

type ParticipateEvent {
  id: ID!
  firstname: String
  lastname: String
  email: String
  phone: String
  createdAt: String
  territory: String
  association: Boolean
  associationName: String
  experience: String
  token: String
}

type Volunteer {
  id: ID!
  firstname: String
  lastname: String
  email: String
  phone: String
  userNote: String
  adminNote: String
  createdAt: String
}

input StageInfos {
  name: String
  startedAt: String
  endedAt: String
  email: String
  phone: String
  address: String
  postCode: String
  city: String
  website: String
  socialNetwork: String
  activity: String
  description: String
  lat: Float
  lng: Float
  entries: [String]
  entriesWithInformation: [EntriesWithInformation]
  shortDescription: String
  contactId: String
  volunteerDescription: String
  referents: [String]
  hasVideoVouaaar: Boolean
  extendStage: Boolean
  memberOf: [String]
  referencingStage: String
  removeReferencingStage: Boolean
  volunteerForm: String
  volunteerAction: VolunteerAction
}
input EntriesWithInformation {
  entryId: ID!
  linkDescription: String
  topSEO: Boolean
}
input EventInfos {
  label: String
  shortDescription: String
  facebookUrl: String
  description: String
  startedAt: String
  endedAt: String
  dateRule: String
  published: Boolean
  entries: [String]
  stages: [String]
  lat: Float
  lng: Float
  address: String
  postCode: String
  city: String
  registerLink: String
  limitPlace: Int
  parentEventId: String
  referencingStage: String
  category: String
  participateButton: Boolean
}
input InputPictureType {
  file: InputFileType
  newpic: Boolean
  deleted: Boolean
  logo: Boolean
  main: Boolean
  partner: Boolean
  id: ID
}
input InputFileType {
  originalPicture: FileUpload
  filename: String
}
input ResetPasswordInfos {
  email: String!
}
input ValidateActionCodeInfos {
  codeId: Int!
  code: Int!
}
input InputOpeningHour {
  days: [InputDay]
  hours: [[String]]
  place: String
}
input InputDay {
  id: String
  day: String
  selected: Boolean
  identifier: String
}
input ContactFormInfos {
  name: String!
  email: String!
  object: String!
  message: String!
  category: String!
  pictures: [InputPictureType]
}
input ParticipateEventInfos {
  firstName: String
  lastName: String
  email: String
  phone: String
  territory: String
  association: Boolean
  associationName: String
  experience: [String]
}

input VolunteerInfos {
  firstName: String!
  name: String!
  email: String!
  phone: String
  message: String
}

input SuggestEventInfos {
  firstName: String
  lastName: String
  email: String
  requesterName: String
  eventName: String!
  stageId: Int!
  message: String
  sendEmail: Boolean
}

input ArticleInfos {
  label: String
  shortDescription: String
  published: Boolean
  stages: [String]
  bannerPrincipalPicture: Boolean
}
