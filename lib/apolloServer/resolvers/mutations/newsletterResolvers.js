import { Newsletter } from 'lib/sequelize/models';

const newsletterResolvers = {
  Query: {},
  Mutation: {
    createNewsletterEmail: async (parent, args, context, info) => {
      try {
        const result = await Newsletter.create({
          email: args.email,
        });
        return result;
      } catch (error) {
        console.log(error);
      }
    },
    createNewsletterUser: async (parent, args, context, info) => {
      try {
        const result = await Newsletter.create();
        await result.setUser(args.userId);
        return result;
      } catch (error) {
        console.log(error);
      }
    },
    // createNewsletterEvent: async (parent, args, context, info) => {},
    // createNewsletterStage: async (parent, args, context, info) => {},
  },
  Newsletter: {
    /* collecton's author */
  },
};

export default newsletterResolvers;
