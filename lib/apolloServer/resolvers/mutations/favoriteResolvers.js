import {
  Stage,
  Event
} from 'lib/sequelize/models';

const favoriteResolver = {
    Mutation: {
        addFavorite: async(parentValue, args, context) => {
          if(args.stageId != null){
          try {
            const stage = await Stage.findOne({ where: { id: args.stageId } });

          if(args.favorite){
            await stage.addFavorites(args.userId);
          }else{
            await stage.removeFavorites(args.userId);
          }
           
            return true;
          } catch (error) {
            console.log(error);
            throw error;
          }
        }else{
            try {
              const event = await Event.findOne({ where: { id: args.eventId } });
              
            if(args.favorite){
              await event.addFavorites(args.userId);
            }else{
              await event.removeFavorites(args.userId);
            }
             
              return true;
            } catch (error) {
              console.log(error);
              throw error;
            }
        }
      },

     
    }
}

export default favoriteResolver;