import {
  Stage,
  Event,
  User,
  Picture,
  Entry,
  Collection,
  EventParticipant
} from 'lib/sequelize/models';
import { Op } from 'sequelize';
import { sendEmail } from 'lib/nodemailer/sendEmail';
import randToken from 'rand-token';
import fs from 'fs';
import { RRule } from 'rrule';
import moment from 'moment';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { GraphQLError } from 'graphql';
import { isAuthenticated } from '../middleware';

const Sequelize = require('sequelize');

const moveNewPictureToEvent= async ( filename, id) => {
  let dirpath = `./public/static/images/event/${id ? id : ''}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/event/${id}/${filename}`;

  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/`+filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path );
  })

  return {
    url,
    filename,
    path,
  };


};

const eventResolvers = {
  Query: {
    events: async (parent, args, context, info) => {
      let events;
      var allEntries = [];
      if (args.entries != undefined && args.entries.length != 0) {
        args.entries.forEach((entries) => {
          if (entries != undefined) {
            entries.forEach((entry) => {
              allEntries.push(entry);
            });
          }
        });
      }

      var whereCondition = {};
      var includeCondition = {};
      var includeStage = {};
      let params = {};
       params.attributes = {
        include: [
          [
            Sequelize.literal(`(SELECT COUNT(*) FROM event_participant uep WHERE uep.eventid = Event.id)`),
            'nbParticipants'
          ]
        ]
      };
      if (args.startingDate) {
        args.startingDate = moment(args.startingDate).startOf('day').toDate();
      }
      //TODO do a and with the search
      if (args.search != undefined && args.search.length != 0) {
        whereCondition['label'] = Sequelize.where(Sequelize.fn('lower', Sequelize.col('`Event`.label')), {
          [Op.like]: `%${args.search.toLowerCase()}%`,
        });
      } else {
        if (args.startingDate) {
          whereCondition['endedAt'] = Sequelize.where(Sequelize.col('`Event`.endedAt'), {
            [Op.gte]: args.startingDate,
          });
        }
        if (args.notFinished) {
          whereCondition['endedAt'] = Sequelize.where(Sequelize.col('`Event`.endedAt'), {
            [Op.gte]: new Date(),
          });
        }
      }

      if (args.favoritesForUser != undefined && args.favoritesForUser != null) {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          where: {
            id: args.favoritesForUser,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          through: {
            attributes: [],
          },
        };
      }

      if (args.stageId) {
        includeStage = {
          model: Stage,
          as: 'stages',
          attributes: ['id'],
          where: {
            id: args.stageId,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeStage = {
          model: Stage,
          as: 'stages',
          attributes: ['id', 'name'],
          through: {
            attributes: [],
          },
        };
      }


      //TODO refacto avec condition pour éviter la duplication (sutout si on rajoute le lieu dans le filtre)
      if (allEntries.length != 0) {
        if (args.startingDate) {
          let eventsBeforeFiltering;

          eventsBeforeFiltering = await Event.findAll({
            where: whereCondition,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                where: {
                  id: {
                    $or: allEntries,
                  },
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Stage,
                as: 'stages',
                attributes: ['id', 'name'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label'],
              },
            ],
          });
          var eventsFilterd = [];

          eventsBeforeFiltering.forEach(async (event) => {
            let isOk = true;
            args.entries.forEach((entries) => {
              if (entries != undefined) {
                let isContained = false;
                event.entries.forEach((eventEntry) => {
                  if (entries.includes('' + eventEntry.id)) {
                    isContained = true;
                  }
                });

                if (entries.length != 0 && !isContained) {
                  isOk = false;
                }
              }
            });
            if (isOk) {
              eventsFilterd.push(event);
            }
          });

          events = eventsFilterd;
        } else {
          events = await Event.findAll({
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Stage,
                as: 'stages',
                attributes: ['id', 'name'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label'],
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        }
      } else {
        if (args.startingDate) {
          events = await Event.findAll({
            where: whereCondition,
            order: [
              [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
            ],
            limit: args.limit ? args.limit : 1000,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Stage,
                as: 'stages',
                attributes: ['id', 'name'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Event,
                as: 'parentEvent',
                attributes: ['id', 'label'],
              },

              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        } else if (args.notFinished) {
          events = await Event.findAll({
            where: whereCondition,
            include: [
              includeCondition,
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
              },
              {
                model: Stage,
                as: 'stages',
                attributes: ['id', 'name'],
                through: {
                  attributes: [],
                },
              },
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
            ],
          });
        } else {
          events = await Event.findAll({
            where: whereCondition,
            order: [
              [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
            ],
            limit: args.limit,
            attributes: {
              include: [
                [
                  Sequelize.literal(`(SELECT COUNT(*) FROM event_participant uep WHERE uep.eventid = Event.id)`),
                  'nbParticipants'
                ]
              ]
            },
            include: [
              includeCondition,
              includeStage,
          
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'logo',
                  'main'
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: Stage,
                as: 'stages',
                attributes: ['id', 'name'],
                through: {
                  attributes: [],
                },
              },
              {
                model: User,
                as: 'favorites',
                attributes: ['id'],
                through: {
                  attributes: [],
                },
              },
              {
                model: Entry,
                as: 'entries',
                attributes: ['id', 'label', 'code', 'color', 'icon'],
                through: {
                  attributes: ['entry_id', 'event_id'],
                },
                include: [
                  {
                    model: Entry,
                    as: 'parentEntry',
                    attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                    include: {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  }
                ]
                
              },
               
              
            ],
          });
        }
      }
      whereCondition = {};
      whereCondition['dateRule'] = {
        [Op.not]: null,
      };
      if (args.search != undefined && args.search.length != 0) {
        whereCondition['label'] = Sequelize.where(Sequelize.fn('lower', Sequelize.col('`Event`.label')), {
          [Op.like]: `%${args.search.toLowerCase()}%`,
        });
      }
      let eventsWithRules = await Event.findAll({

        where: whereCondition,
        order: [
          [args.sort ? args.sort : 'startedAt', args.way ? args.way : 'ASC'],
        ],
        limit: args.limit,
        include: [
          includeCondition,
          includeStage,
          {
            model: Entry,
            as: 'entries',
            attributes: ['id', 'label', 'code', 'color', 'icon'],
            through: {
              attributes: ['entry_id', 'event_id'],
            },
            include: [
              {
                model: Entry,
                as: 'parentEntry',
                attributes: ['id', 'label', 'position', 'code', 'color', 'icon'],
                include: {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              }
            ]
          },
          {
            model: Stage,
            as: 'stages',
            attributes: ['id', 'name'],
            through: {
              attributes: [],
            },
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'logo',
              'main',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            model: Event,
            as: 'parentEvent',
            attributes: ['id', 'label'],
          },
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
        ],
      });


      eventsWithRules.forEach((eventWithRules) => {

        const startDate = moment(parseInt(eventWithRules.startedAt.getTime()));
        const dateRule = eventWithRules.dateRule;


        const rrule = RRule.fromString('DTSTART:' + startDate.format('YYYYMMDD[T]hhmmss[Z]') + '\nRRULE:' + dateRule);
        let startDateSearch;
        if (args.startingDate) {
          startDateSearch = Date.parse(args.startingDate);
        }
        if (args.notFinished) {
          startDateSearch = new Date();
        }
        let endDate;
        if (startDateSearch) {
          endDate = rrule.after(new Date(startDateSearch));
        }



        if ((!endDate || endDate > startDateSearch) && !events.some(e => e.id === eventWithRules.id)) {
          events.push(eventWithRules);
        }

      });

      if (args.canAdmin) {
        if (context.req?.session?.user?.id && context.req?.session?.user?.role === 'admin') {
          return events;
        } else if (context.req?.session?.user?.id) {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Stage,
                as: 'adminstages',
              },
            ]
          });

          const u = user.get({ plain: true});
          const stages = u.adminstages.map(s => s.id);

          return events
            .map(event => event.get({ plain: true}))
            .filter(event => event.stages
              .map(s => s.id)
              .some(s => stages.includes(s))
            );

        } else {
          return [];
        }
      }

      return events;
    },
    event: async (parent, args, context, info) => {
      const event = await Event.findOne({
        where: { id: args.id },
        include: [
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
          {
            model: Stage,
            as: 'stages',
            attributes: ['id', 'name', 'startedAt', 'volunteerAction', 'volunteerForm'],
            through: {
              attributes: [],
            },

            include: [
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'main',
                  'logo',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname'],
                through: {
                  attributes: [],
                },
              },
            ],
          },
          {
            model: User,
            as: 'participants',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          }, {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'main',
              'logo',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            model: Entry,
            as: 'entries',
            include: [
              {
                model: Entry,
                as: 'parentEntry',
                attributes: ['id', 'label', 'position'],
                include: {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              },
              {
                model: Collection,
                as: 'collection',
                attributes: ['id', 'code', 'label', 'position'],
              },
            ],
          },
          {
            model: Event,
            as: 'parentEvent',
            attributes: ['id', 'label', 'startedAt', 'endedAt'],
            include: [
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'main',
                  'logo',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
            ]
          },
          {
            model: Event,
            as: 'subEvents',
            separate: true,
            attributes: ['id', 'label', 'startedAt', 'endedAt', 'city', 'address'],
          },

        ],
      });
      return event;
    },
    eventParticipants: async (parent, args, context, info) => {
      let participants = [];
        participants = EventParticipant.findAll({
          include: [
            {
              model: Event,
              as: 'event',
              where: {
                id: args.eventId
              }
            },
          ],
        });
      return participants;
    },

    participants: async (parent, args, context, info) => {
      let participants = [];

      if (context?.req?.session?.user) {
        participants = User.findAll({
          include: [
            {
              model: Event,
              as: 'events',
              where: {
                id: args.eventId
              }
            },
          ],
          attributes: {
            include: [
              [
                Sequelize.literal(
                  `(SELECT createdAt FROM user_event_participant uep WHERE uep.event_id = ${args.eventId} AND uep.user_id = User.id)`
                ),
                'participatedAt'
              ]
            ]
          }
        });
      }

      return participants;
    }
  },
  Mutation: {
    createEvent: async (parent, args, context) => {
      try {
        // check si le user est admin de l'étape parente de l'event
        if (context.req?.session?.user?.role !== 'admin') {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Stage,
                as: 'adminstages',
              },
            ]
          });

          const u = user.get({ plain: true});
          const stages = u.adminstages.map(s => s.id);

          if (!stages.includes(args.stageId)) {
            throw new GraphQLError('Not allowed to create this event');
          }
        }

        const event = await Event.create({
          label: args.eventInfos.label,
          shortDescription: args.eventInfos.shortDescription,
          facebookUrl: args.eventInfos.facebookUrl,
          description: args.description,
          startedAt: args.eventInfos.startedAt,
          endedAt: args.eventInfos.endedAt,
          dateRule: args.eventInfos.dateRule,
          published: args.eventInfos.published,
          lat: args.eventInfos.lat,
          lng: args.eventInfos.lng,
          address: args.eventInfos.address,
          postCode: args.eventInfos.postCode,
          city: args.eventInfos.city,
          practicalInfo: args.practicalInfo,
          registerLink: args.eventInfos.registerLink,
          limitPlace: args.eventInfos.limitPlace,
          event_id: args.eventInfos.parentEventId,
          category: args.eventInfos.category,
          participateButton : args.eventInfos.participateButton,
        });
        await event.addStage(args.stageId);

        if (args.mainPictures) {
          await uploadImage(
            args.mainPictures,
            event.id,
          );
        }
        if (args.pictures) {
          await uploadImage(args.pictures, event.id);
        }

        const eventWithStages = await Event.findOne({
          where: { id: event.id },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name', 'startedAt'],
              through: {
                attributes: [],
              },
            },
          ],
        });
        return eventWithStages;
      } catch (err) {
        console.log(err);
      }
    },
    editEvent: async (parent, args, context, infos) => {
      try {
        const getEvent = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
            },
          ],
        });

        // check si le user est admin d'une étape qui est admin de l'event
        if (context.req?.session?.user?.role !== 'admin') {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Stage,
                as: 'adminstages',
              },
            ]
          });

          const u = user.get({ plain: true});
          const stages = u.adminstages.map(s => s.id);

          if (getEvent.get({ plain: true}).stages.map(s => s.id).every(s => !stages.includes(s))) {
            throw new GraphQLError('Not allowed to edit this event');
          }
        }

        const event = await Event.update(
          {
            label: args.eventInfos.label,
            shortDescription: args.eventInfos.shortDescription,
            facebookUrl: args.eventInfos.facebookUrl,
            description: args.description,
            startedAt: args.eventInfos.startedAt,
            endedAt: args.eventInfos.endedAt,
            published: args.eventInfos.published,
            lat: args.eventInfos.lat,
            lng: args.eventInfos.lng,
            address: args.eventInfos.address,
            postCode: args.eventInfos.postCode,
            city: args.eventInfos.city,
            practicalInfo: args.practicalInfo,
            registerLink: args.eventInfos.registerLink,
            limitPlace: args.eventInfos.limitPlace,
            event_id: args.eventInfos.parentEventId,
            dateRule: args.eventInfos.dateRule,
            category: args.eventInfos.category,
            participateButton : args.eventInfos.participateButton,
          },
          {
            where: {
              id: args.eventId,
            },
          },
        );


        const result = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Entry,
              as: 'entries',
              attributes: ['id'],
            },
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
            },

          ],
        });

        if (args.mainPictures) {
          await uploadImage(args.mainPictures, result.id);
        }
        if (args.pictures) {
          await uploadImage(args.pictures, result.id);
        }

        //TODO mettre en commum avec la methode event
        const eventReloaded = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'main',
                    'logo',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
                {
                  model: User,
                  as: 'referents',
                  attributes: ['id', 'surname', 'lastname'],
                  through: {
                    attributes: [],
                  },
                },
              ],
            },
            {
              model: User,
              as: 'participants',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            }, {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'main',
                'logo',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },
                },
                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
            },
          ],
        });

        return eventReloaded;
      } catch (err) {
        console.log(err);
      }
    },
    addEventParticipate: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [{

            model: Stage,
            as: 'stages',
            attributes: ['id', 'name'],
            through: {
              attributes: [],
            },
            include: [
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname', 'email'],
                through: {
                  attributes: [],
                },
              },
            ],
          }],
        });

        await event.addParticipants(args.userId);

        var emailReferents = '';

        event.stages.map((stage) => {

          stage.referents.map((referent) => {
            emailReferents += referent.email + ',';
          });
        });

        const emailTitle =
          "Nouveau participant à l'événement " + event.label;

        const emailTemplate = 'newActionParticipate';
        const user = await User.findByPk(args.userId);

        const data = {
          eventName: event.label,
          idEvent: args.eventId,
          firstNameVolunteer: user.surname,
          lastNameVolunteer: user.lastname,
          emailVolunteer: user.email,
          url: process.env.CLIENT_URL + "/admin/events"
        };

        await sendEmail(process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data); //emailReferents+process.env.EMAIL_CONTACT



        return true;
      } catch (error) {
        console.log(error);
      }
    },
    addParticipateEvent: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [{

            model: Stage,
            as: 'stages',
            attributes: ['id', 'name'],
            through: {
              attributes: [],
            },
            include: [
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname', 'email'],
                through: {
                  attributes: [],
                },
              },
            ],
          }],
        });

        const eventParticipant = await EventParticipant.create({
          eventId: args.eventId,
          firstname: args.participateEventInfos.firstName,
          lastname: args.participateEventInfos.lastName,
          email: args.participateEventInfos.email,
          phone: args.participateEventInfos.phone,
          territory: args.participateEventInfos.territory,
          association: args.participateEventInfos.association,
          associationName: args.participateEventInfos.associationName,
          experience: JSON.stringify(args.participateEventInfos.experience),
          token: randToken.generate(16)
        });
     //   await event.addEvent(eventParticipant);



        var emailReferents = '';

        event.stages.map((stage) => {

          stage.referents.map((referent) => {
            emailReferents += referent.email + ',';
          });
        });

        const emailTitle =
          "Inscription à " + event.label+ " le " + event.startedAt.toLocaleDateString('fr-FR', {  weekday: 'long', day: '2-digit', month: '2-digit' });

        const emailTemplate = 'newActionParticipate';

        const data = {
          eventName: event.label,
          idEvent: args.eventId,
          firstName: args.participateEventInfos.firstName,
          date : event.startedAt.toLocaleDateString('fr-FR', {  weekday: 'long',day: '2-digit', month: '2-digit' }),
          location : event.address,
          hourStarted : event.startedAt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit', timeZone: 'Europe/Paris'  }),
          hourEnded : event.endedAt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' , timeZone: 'Europe/Paris' }),
          link: process.env.CLIENT_URL + "/etape/"+event.stages[0].name+"/activite/" + args.eventId,
          unregisterlink: process.env.CLIENT_URL + "/etape/"+event.stages[0].name+"/activite/desinscription/" + args.eventId+"/"+eventParticipant.token,
        };

        await sendEmail(args.participateEventInfos.email, emailTitle, emailTemplate, data); //emailReferents+process.env.EMAIL_CONTACT
  

        return true;
      } catch (error) {
        if(error.name === 'SequelizeUniqueConstraintError'){
          error = 'Vous êtes déjà inscrit à cet événement';
        }
        throw new GraphQLError(error);
      }
    },
    removeParticipateEvent: async (parent, args, context, infos) => {
      try {


        const eventParticipant = await EventParticipant.findOne({
          where: { token: args.token, eventId: args.eventId },
        });

        await eventParticipant.destroy();

        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [{

            model: Stage,
            as: 'stages',
            attributes: ['id', 'name'],
            through: {
              attributes: [],
            },
            include: [
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname', 'email'],
                through: {
                  attributes: [],
                },
              },
            ],
          }],
        });




        const emailTitle =
          "Désinscription à " + event.label+ " le " + event.startedAt.toLocaleDateString('fr-FR', { weekday: 'long', day: '2-digit', month: '2-digit' });

        const emailTemplate = 'removeParticipantEvent';

        const data = {
          eventName: event.label,
          idEvent: args.eventId,
          firstName: eventParticipant.firstname,
          date: event.startedAt.toLocaleDateString('fr-FR', { weekday: 'long', day: '2-digit', month: '2-digit' }),
          location: event.address,
          hourStarted: event.startedAt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit', timeZone: 'Europe/Paris' }),
          hourEnded: event.endedAt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit', timeZone: 'Europe/Paris' }),
          link: process.env.CLIENT_URL + "/etape/" + event.stages[0].name + "/activite/" + args.eventId,
        };

        await sendEmail(eventParticipant.email, emailTitle, emailTemplate, data); //emailReferents+process.env.EMAIL_CONTACT



        return true;
      } catch (error) {
        console.log(error);
        throw new GraphQLError(error);
      }
    },



    removeEventParticipate: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({ where: { id: args.eventId } });

        await event.removeParticipants(args.userId);
        return true;
      } catch (error) {
        console.log(error);
      }
    },
    deleteEvent: async (parent, args, context, infos) => {
      try {
        const event = await Event.findOne({
          where: { id: args.eventId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id', 'event_id'],
              },
            },
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
          ],
        });

        // checker si le user est admin d'une étape qui est admin de l'event
        if (context.req?.session?.user?.role !== 'admin') {
          const user = await User.findOne({
            where: { id: context.req?.session?.user?.id },
            include: [
              {
                model: Stage,
                as: 'adminstages',
              },
            ]
          });

          const u = user.get({ plain: true});
          const stages = u.adminstages.map(s => s.id);

          if (event.get({ plain: true}).stages.map(s => s.id).every(s => !stages.includes(s))) {
            throw new GraphQLError('Not allowed to delete this event');
          }
        }

        const referentsDeletion = event.referents.map(async (referent) => {
          await event.removeReferents(referent);
        });
        await Promise.all(referentsDeletion);
        const stagesDeletion = event.stages.map(async (stage) => {
          await event.removeStages(stage);
        });
        await Promise.all(stagesDeletion);

        const picturesDeletion = event.pictures.map(async (picture) => {
          await event.removePictures(picture);
        });
        await Promise.all(picturesDeletion);

        const result = await Event.destroy({
          where: {
            id: args.eventId,
          },
        });

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
  Event: {
    /* collecton's author */
  },
};
const uploadImage = async (pictures, eventID) => {
  //upload files
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true

            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToEvent(
                image.file.filename,
                `${eventID}`
              )])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${eventID}-${fileToken}-original`,
                  position: index,
                  eventID: eventID,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    const uploadedFiles = await uploadedFilesPromise;

    return uploadedFiles.map(async (file) => {
      if (!file.newpic && file.deleted) {
        // supprimer une image déja existante

        return new Promise((resolve, reject) => {
          Picture.findByPk(file.id).then((picture) => {
            fs.unlink(
              `/static/images/activite/${eventID}/${picture.originalPictureFilename}.jpeg`,
              () => {
                picture.destroy().then((value) => {
                  resolve({
                    id: file.id,
                    destroyed: value,
                  });
                });
              },
            );
          });
        });
      } else {
        // sinon traiter les images à updater

        if (file.newpic) {
          if (file.originalPictureFilename) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setEvent(eventID);
            return result;
          } else {
            return null;
          }

        } else {
          // si l'image est deja existante, il faut updater sa position
          const result = Picture.update(
            file,
            // {
            // position : file.position
            // }
            { where: { id: file.id } },
          );
          // position = position +1;
          return result;
        }
      }
    });

    const result = await Promise.all(_);
    /*  result.map((picture) => {
        if(picture.dataValues){
        picture.dataValues.setStage(stageId)
        }
    });
*/

    //console.log(result);
  }
};

export default composeResolvers(eventResolvers, {
  'Mutation.createEvent': [isAuthenticated()],
  'Mutation.editEvent': [isAuthenticated()],
  'Mutation.deleteEvent': [isAuthenticated()],
});
