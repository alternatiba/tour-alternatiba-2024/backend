import { Stage, Event, Article } from 'lib/sequelize/models';
const { Op } = require('sequelize');
const Sequelize = require('sequelize');

const searchResolvers = {
  Query: {
    search: async (parent, args, context, info) => {
      var stages;
      var events;
      var articles;
      const result = {}
      try {
      if(args.searchStage){
        stages = await Stage.findAll({
          where: {
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('activity')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
            ],
            isValidated : true
          }
        });
        result.stages=stages;
      }
      if(args.searchEvent){
        events = await Event.findAll({
          where: {
            [Op.and]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('label')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.col('endedAt'), {
                [Op.gte]: new Date(),
                }),
            ],
          }
        });
        result.events=events;
      }
      if(args.searchArticle){ 
        articles = await Article.findAll({
          where: {
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('label')), {
              [Op.like]: `%${args.searchValue.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
                [Op.like]: `%${args.searchValue.toLowerCase()}%`,
                }),
              ],
          }
        });
        result.articles=articles;
      }
      
       
        return result;
      } catch (error) {
        console.log(error);
      }
    }
  },
  Mutation: {

  },
};

export default searchResolvers;
