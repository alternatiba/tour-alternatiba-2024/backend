import {
  Article,
  User,
  Picture,
  Stage,
} from 'lib/sequelize/models';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import randToken from 'rand-token';
import fs from 'fs';
import { isAdmin, isAuthenticated } from '../middleware';
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const moveNewPictureToArticle = async ( filename, id) => {
  let dirpath = `./public/static/images/article/${id ? id : ''}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/article/${id}/${filename}`;

  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/`+filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path );
  })

  return {
    url,
    filename,
    path,
  };
};

const articleResolvers = {
  Query: {
    article: async (parent, args, context, info) => {
      const article = await Article.findOne({
        where: { id: args.id },
        include: [
          {
            model: Stage,
            as: 'stages',
            attributes: ['id', 'name'],
            through: {
              attributes: ['article_id', 'stage_id'],
            },
            include: [
              {
                separate: true,
                model: Picture,
                as: 'pictures',
                attributes: [
                  'id',
                  'label',
                  'originalPicturePath',
                  'originalPictureFilename',
                  'position',
                  'main',
                  'logo',
                ],
                orderBy: [
                  // ['activated','DESC'],
                  ['position', 'ASC'],
                ],
              },
              {
                model: User,
                as: 'referents',
                attributes: ['id', 'surname', 'lastname'],
                through: {
                  attributes: [],
                },
              },
            ]

          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'main',
              'logo',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
        ],
      });
      return article;
    },
    articles: async (parent, args, context, info) => {
      let whereCondition = {};

      var includeCondition = {
        model: Stage,
        as: 'stages',
        attributes: ['id'],
        through: {
          attributes: [],
        },
      };

      if (args.stageId) {
        includeCondition = {
          model: Stage,
          as: 'stages',
          attributes: ['id'],
          where: {
            id: args.stageId,
          },
          through: {
            attributes: [],
          },
        };
      }

      if (args.search) {
        whereCondition = {
          where: {
            [Op.or]: {
              label: {
                [Op.like]: `%${args.search}%`
              },
              shortDescription: {
                [Op.like]: `%${args.search}%`
              },
            }
          }
        }
      }
      if (args.tag) {
        whereCondition = {
          where: {
            [Op.or]: {
              label: {
                [Op.like]: `%${args.search}%`
              },
              shortDescription: {
                [Op.like]: `%${args.search}%`
              },
            }
          }
        }
      }

      const articles = await Article.findAll({
        order: [
          [args.sort ? args.sort : 'createdAt', args.way ? args.way : 'DESC'],
        ],
        include: [
          includeCondition,
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'main',
              'logo',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
        ],
        limit: args.limit,
        ...whereCondition
      });

      return articles;
    }
  },
  Mutation: {
    createArticle: async (parent, args, context, infos) => {
      try {
        const article = await Article.create({
          label: args.articleInfos.label,
          shortDescription: args.articleInfos.shortDescription,
          content: args.content,
          published: args.articleInfos.published,
        });

        if (args.mainPictures) {
          await uploadImage(
            args.mainPictures,
            article.id,
          );
        }
        if (args.pictures) {
          await uploadImage(
            args.pictures,
            article.id,
          );
        }

        return article;
      } catch (err) {
        console.log(err);
      }
    },
    editArticle: async (parent, args, context, infos) => {
      try {
        await Article.update(
          {
            label: args.articleInfos.label,
            shortDescription: args.articleInfos.shortDescription,
            content: args.content,
            description: args.description,
            published: args.articleInfos.published,
            bannerPrincipalPicture : args.articleInfos.bannerPrincipalPicture,
          },
          {
            where: {
              id: args.articleId,
            },
          },
        );

        const result = await Article.findOne({
          where: { id: args.articleId },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
            },

          ],
        });

        if (args.articleInfos.stages) {
          var existingStages = [];
          result.stages.forEach(async (stage) => {
            existingStages.push(stage.id);

            if (!args.articleInfos.stages.includes(stage.id.toString())) {
              await result.removeStage(stage);
            }
          });

          args.articleInfos.stages.forEach(async (stageId) => {
            if (!existingStages.includes(parseInt(stageId))) {
              await result.addStage(stageId);
            }
          });
        }

        if (args.logoPictures) {
          await uploadImage(args.logoPictures, result.id);
        }

        if (args.mainPictures) {
          await uploadImage(args.mainPictures, result.id);
        }
        if (args.pictures) {
          await uploadImage(args.pictures, result.id);
        }

        //TODO mettre en commum avec la methode article
        const articleReloaded = await Article.findOne({
          where: { id: args.articleId },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: ['article_id', 'stage_id'],
              },
              include: [
                {
                  model: User,
                  as: 'referents',
                  attributes: ['id', 'surname', 'lastname'],
                  through: {
                    attributes: ['user_id', 'stage_id'],
                  },
                },
              ],
            },
          ],
        });

        return articleReloaded;
      } catch (err) {
        console.log(err);
      }
    },
    deleteArticle: async (parent, args, context, infos) => {
      try {
        const article = await Article.findOne({
          where: { id: args.articleId },
          include: [
            {
              model: Stage,
              as: 'stages',
              attributes: ['id', 'name'],
              through: {
                attributes: [],
              },
            }
          ],
        });

        const stagesRemode = article.stages.map(async (stage) => {
          await article.removeStages(stage);
        });
        await Promise.all(stagesRemode);



        const result = await Article.destroy({
          where: {
            id: args.articleId,
          },
        });

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
  Article: {
    // resolver code for Article fields
  }
};

const uploadImage = async (pictures, articleID) => {
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true

            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToArticle(
                image.file.filename,
                `${articleID}`
              )])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${articleID ? (articleID) : ''}-${fileToken}-original`,
                  position: index,
                  articleID: articleID,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    if (articleID) {
      const uploadedFiles = await uploadedFilesPromise;

      return uploadedFiles.map(async (file) => {
        if (!file.newpic && file.deleted) {
          // supprimer une image déja existante

          return new Promise((resolve, reject) => {
            Picture.findByPk(file.id).then((picture) => {
              fs.unlink('./public' + picture.originalPicturePath,
                () => {
                  picture.destroy().then((value) => {
                    resolve({
                      id: file.id,
                      destroyed: value,
                    });
                  });
                },
              );
            });
          });
        } else {
          // sinon traiter les images à updater

          if (file.newpic) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setArticle(articleID);
            return result;
          } else {
            // si l'image est deja existante, il faut updater sa position
            const result = Picture.update(
              file,
              // {
              // position : file.position
              // }
              { where: { id: file.id } },
            );
            // position = position +1;
            return result;
          }
        }
      });
    }
  }
};

export default composeResolvers(articleResolvers, {
  'Mutation.createArticle': [isAuthenticated(), isAdmin()],
  'Mutation.editArticle': [isAuthenticated(), isAdmin()],
  'Mutation.deleteArticle': [isAuthenticated(), isAdmin()],
});
