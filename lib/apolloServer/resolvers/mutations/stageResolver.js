
import {
  Stage,
  Event,
  User,
  Picture,
  Entry,
  Collection,
  StageEntries,
  OpeningHours,
  StageVolunteer
} from 'lib/sequelize/models';
import { DataTypes, Op } from 'sequelize';
import randToken from 'rand-token';
import fs from 'fs';
import { sendEmail } from 'lib/nodemailer/sendEmail';
import moment from 'moment';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import { isAdmin, isAuthenticated } from '../middleware';
const Sequelize = require('sequelize');


const moveNewPictureToStage = async (filename, id) => {
  let dirpath = `./public/static/images/stage/${id}`;
  let path = dirpath + `/${filename}`;
  let url = `/static/images/stage/${id}/${filename}`;
  fs.mkdirSync(dirpath, { recursive: true });
  fs.rename(`./public/static/images/newUpload/` + filename, path, function (err) {
    if (err) throw err
    console.log('Successfully moved - new file' + path);
  })

  return {
    url,
    filename,
    path,
  };


};
const transformNameToUrl = (stageName) => {


  // Remove any non-alphanumeric characters from the stageName
  const alphanumericName = replaceAccents(stageName).replace(/[^a-zA-Z0-9\s+/g]/g, '');
  // Convert the name to lowercase and replace spaces with hyphens
  const urlName = alphanumericName.toLowerCase().replace(/\s+/g, '-');
  // Return the URL
  return urlName;
};
const replaceAccents = (str) => {
  const accents = [
    /[\300-\306]/g, /[\340-\346]/g, // A, a
    /[\310-\313]/g, /[\350-\353]/g, // E, e
    /[\314-\317]/g, /[\354-\357]/g, // I, i
    /[\322-\330]/g, /[\362-\370]/g, // O, o
    /[\331-\334]/g, /[\371-\374]/g, // U, u
    /[\321]/g, /[\361]/g, // N, n
    /[\307]/g, /[\347]/g, // C, c
  ];
  const replacements = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
  let result = str;
  for (let i = 0; i < accents.length; i++) {
    result = result.replace(accents[i], replacements[i]);
  }
  return result;
};

const isStageAdmin = (stage, context) => {
  if (context.req?.session?.user?.id && context.req?.session?.user?.role === 'admin') {
    return true;
  } else if (context.req?.session?.user?.id && stage.referents.map(r => r.id).includes(context.req?.session?.user?.id)) {
    return true;
  }

  return false;
}

const stageResolvers = {
  Query: {
    stages: async (parent, args, context, info) => {
      const Sequelize = require('sequelize');

      let stages;
      var whereCondition = {};
      var includeCondition = {};
      whereCondition['isValidated'] = true;
      //   debugger;
      if (args.search != undefined) {
        whereCondition = {
          ...whereCondition, ...{
            [Op.or]: [
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('shortDescription')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.fn('lower', Sequelize.col('activity')), {
                [Op.like]: `%${args.search.toLowerCase()}%`,
              }),
              Sequelize.where(Sequelize.col('`Stage`.postCode'), {
                [Op.like]: `${args.search.toLowerCase()}%`,
              }),
            ]
          }
        };
      }

      if (args.favoritesForUser != undefined && args.favoritesForUser != null) {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          where: {
            id: args.favoritesForUser,
          },
          through: {
            attributes: [],
          },
        };
      } else {
        includeCondition = {
          model: User,
          as: 'favorites',
          attributes: ['id'],
          through: {
            attributes: [],
          },
        };
      }

      if (args.stagesCollective != undefined && args.stagesCollective.length != 0) {
        includeCondition = {
          model: Stage,
          as: 'memberOf',
          attributes: ['id'],
          where: {
            id: {
              [Op.in]: args.stagesCollective,
            },
          },
          through: {
            attributes: [],
          },
        };
      }

      var allEntries = [];
      if (args.entries != undefined && args.entries.length != 0) {
        args.entries.forEach((entries) => {
          if (entries != undefined) {
            entries.forEach((entry) => {
              allEntries.push(entry);
            });
          }
        });
      }
      if (allEntries.length != 0) {

        let stagesBeforeFiltering;

        stagesBeforeFiltering = await Stage.findAll({
          where: whereCondition,
          include: [
            includeCondition,
            {
              model: Entry,
              as: 'entries',
              attributes: ['id', 'code', 'label', 'icon', 'description', 'color'],
              where: {
                id: {
                  $or: allEntries,
                },
              },
              through: {
                attributes: ['entry_id', 'stage_id'],
              },
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position', 'color'],
                },
              ]
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },

          ],
        });
        var stageFilterd = [];
debugger;
        stagesBeforeFiltering.forEach(async (stage) => {
          let isOk = true;
          args.entries.forEach((entries) => {
            if (entries != undefined) {
              let isContained = false;
              stage.entries.forEach((stageEntry) => {
                if (entries.includes('' + stageEntry.id)) {
                  isContained = true;
                }
              });

              if (entries.length != 0 && !isContained) {
                isOk = false;
              }
            }
          });
          if (isOk) {
            stageFilterd.push(stage);
          }
        });

        stages = stageFilterd;
      } else {
        stages = await Stage.findAll({
          where: whereCondition,
          order: [
            [args.sort ? args.sort : 'name', args.way ? args.way : 'ASC'],
          ],
          limit: args.limit,
          include: [
            includeCondition,
            {
              model: Entry,
              as: 'entries',
              attributes: ['id', 'code', 'label', 'icon', 'description', 'color'],
              through: {
                attributes: ['entry_id', 'stage_id'],
              },
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position', 'color'],
                },
              ]
            },
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              model: User,
              as: 'favorites',
              attributes: ['id'],
              through: {
                attributes: [],
              },
            },
          ],
        });
      }

      if (args.canAdmin) {
        return stages.filter(stage => isStageAdmin(stage, context));
      }

      return stages;
    },
    stagesCollective: async (parent, args, context, info) => {
      let stages;
      stages = await Stage.findAll({
        include: [
          {
            model: Stage,
            as: 'members',
            attributes: ['id'],
          }
        ],
        where: {
          '$members.id$': {
            [Op.gt]: 0
          }
        },
        orderBy: [
          ['createdAt', 'DESC'],
        ]
      });

      stages = stages.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1));

      return stages;
    },
    stage: async (parent, args, context, info) => {
      const startQuery = moment();
      var whereCondition = {};
      if (args.id != undefined) {
        whereCondition['id'] = args.id;
      } else if (args.name != undefined) {
        if (args.name != undefined) {
          whereCondition['name'] = args.name;
        }
      }
      const stage = await Stage.findOne({
        where: whereCondition,
        include: [
          {
            model: Entry,
            as: 'entries',
            include: [
              {
                model: Entry,

                as: 'parentEntry',
                attributes: ['id', 'label', 'position'],
                include: {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },

              },
              {
                model: Collection,

                as: 'collection',
                attributes: ['id', 'code', 'label', 'position'],
              },
            ],
            order: [{ model: StageEntries }, 'topSEO', 'ASC'],
          },

          {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname'],
            through: {
              attributes: [],
            },
          },
          {
            separate: true,
            model: Picture,
            as: 'pictures',
            attributes: [
              'id',
              'label',
              'originalPicturePath',
              'originalPictureFilename',
              'position',
              'logo',
              'main',
              'partner',
            ],
            orderBy: [
              // ['activated','DESC'],
              ['position', 'ASC'],
            ],
          },
          {
            separate: true,
            model: OpeningHours,
            as: 'openingHours',
            attributes: ['id', 'days', 'hours', 'place'],
          },
          {
            model: User,
            as: 'favorites',
            attributes: ['id'],
            through: {
              attributes: [],
            },
          },
          {
            model: Stage,
            as: 'memberOf',
            attributes: ['id', 'name'],
            include: [{
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            }
            ],
            through: {
              attributes: [],
            },
          },
          {
            model: Stage,
            as: 'members',
            attributes: ['id', 'name', 'lat', 'lng', 'address', 'postCode', 'city', 'phone', 'email', 'website', 'socialNetwork', 'activity', 'description', 'shortDescription', 'volunteerDescription', 'extendStage', 'contact_id', 'isValidated', 'url'],
            include: [{
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo'
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            }
            ]
          },
          {
            model: Stage,
            as: 'referencingStage',
            attributes: ['id', 'name'],

          }
        ],
      });
      const endQuery = moment();
      //console.log( "query in " + endQuery.diff(startQuery,'seconds')+'s');
      return stage;
    },
  },
  Mutation: {
    /* mutation to create user's infos */
    createStage: async (parent, args, context, info) => {

      try {
        const stageCreated = await Stage.create({
          name: args.stageInfos.name,
          email: args.stageInfos.email,
          phone: args.stageInfos.phone,
          address: args.stageInfos.address,
          postCode: args.stageInfos.postCode,
          city: args.stageInfos.city,
          website: args.stageInfos.website,
          socialNetwork: args.stageInfos.socialNetwork,
          activity: args.stageInfos.activity,
          description: args.description,
          shortDescription: args.stageInfos.shortDescription,
          lat: args.stageInfos.lat,
          lng: args.stageInfos.lng,
          volunteerDescription: args.volunteerDescription,
          referencing_stage_id: args.stageInfos.referencingStage,
          startedAt: args.stageInfos.startedAt,
          endedAt: args.stageInfos.endedAt,
          isValidated: true,
          volunteerAction: args.stageInfos.volunteerAction,
          volunteerForm: args.stageInfos.volunteerForm,
        });

        let entriesWithInformationAlreadeyCreated = [];
        if (args.stageInfos.entriesWithInformation) {
          entriesWithInformationAlreadeyCreated = args.stageInfos.entriesWithInformation.map(entryWithInformation => { return entryWithInformation.entryId })
          const resultAddEntriesInformation = args.stageInfos.entriesWithInformation.map(async (entryWithInformation) => {
            stageCreated.StageEntries = {
              topSEO: entryWithInformation.topSEO,
              linkDescription: entryWithInformation.linkDescription
            }
            await stageCreated.addEntry(entryWithInformation.entryId, { through: { topSEO: entryWithInformation.topSEO, linkDescription: entryWithInformation.linkDescription } })
            entriesWithInformationAlreadeyCreated.push(entryWithInformation.entryId);

          });
          Promise.all(resultAddEntriesInformation);
        }



        if (args.stageInfos.entries) {
          args.stageInfos.entries.forEach(async (entry) => {
            if (parseInt(entry) > 0) {
              if (!entriesWithInformationAlreadeyCreated.includes(entry)) {
                await stageCreated.addEntry(entry);
              }
            }
          });
        }

        if (args.stageInfos.contactId) {
          stageCreated.contact_id = args.stageInfos.contactId;
        } else {
          stageCreated.contact_id = context.req?.session?.user?.id;
        }

        if (args.stageInfos.extendStage != undefined) {
          stageCreated.extendStage = args.stageInfos.extendStage;
        }
        await stageCreated.save();

        // await stageCreated.addReferent(user.id);
        // await stageCreated.save();
        const result = await Stage.findOne({
          where: { id: stageCreated.id },
          include: [
            {
              model: Stage,
              as: 'memberOf',
              attributes: ['id'],
            },
          ],
        });


        if (args.stageInfos.memberOf) {
          var existingMemberOf = [];
          result.memberOf.forEach(async (memberOf) => {
            existingMemberOf.push(memberOf.id);

            if (!args.stageInfos.memberOf.includes(memberOf.id.toString())) {
              await result.removeMemberOf(memberOf);
            }
          });

          args.stageInfos.memberOf.forEach(async (memberOfId) => {
            if (!existingMemberOf.includes(parseInt(memberOfId))) {
              await result.addMemberOf(memberOfId);
            }
          });
        }

        if (args.openingHours) {
          args.openingHours.forEach(async (openingHourdto) => {
            const openingHour = await OpeningHours.create({
              days: openingHourdto.days,
              hours: openingHourdto.hours,
              place: openingHourdto.place,
            });
            await openingHour.setStage(result.id);
          });


          stageCreated.contact_id = args.stageInfos.contactId;
        }

        if (args.logoPictures) {
          const pictures = await managePicture(
            args.logoPictures,
            stageCreated.id,
          );
        }

        if (args.mainPictures) {
          const pictures = await managePicture(
            args.mainPictures,
            stageCreated.id,
          );
        }

        if (args.partnerPictures) {
          const pictures = await managePicture(
            args.partnerPictures,
            stageCreated.id,
          );
        }

        if (args.pictures) {
          const pictures = await managePicture(args.pictures, stageCreated.id);
        }

        return result;
      } catch (err) {
        console.log(err);
      }

      return result;
    },


    editStage: async (parent, args, context, info) => {
      try {
        const stageEdited = await Stage.update(
          {
            name: args.stageInfos.name,
            email: args.stageInfos.email,
            url: transformNameToUrl(args.stageInfos.name),
            phone: args.stageInfos.phone,
            address: args.stageInfos.address,
            postCode: args.stageInfos.postCode,
            city: args.stageInfos.city,
            website: args.stageInfos.website,
            socialNetwork: args.stageInfos.socialNetwork,
            activity: args.stageInfos.activity,
            description: args.description,
            lat: args.stageInfos.lat,
            lng: args.stageInfos.lng,
            volunteerDescription: args.volunteerDescription,
            shortDescription: args.stageInfos.shortDescription,
            startedAt: args.stageInfos.startedAt,
            endedAt: args.stageInfos.endedAt,
            volunteerAction: args.stageInfos.volunteerAction,
            volunteerForm: args.stageInfos.volunteerForm,
          },
          {
            where: {
              id: args.stageId,
            },
          },
        );
        if (args.stageInfos.removeReferencingStage) {
          await Stage.update(
            {
              referencing_stage_id: null
            },
            {
              where: {
                id: args.stageId,
              },
            },
          );
        }

        const result = await Stage.findOne({
          where: { id: args.stageId },
          include: [
            {
              model: Entry,
              as: 'entries',
              attributes: ['id'],
            },
            {
              separate: true,
              model: OpeningHours,
              as: 'openingHours',
              attributes: ['id', 'days', 'hours', 'place'],
            },
            {
              model: User,
              as: 'referents',
              attributes: ['id'],
            },
            {
              model: Stage,
              as: 'memberOf',
              attributes: ['id'],
            },
          ],
        });
        if (args.stageInfos.hasVideoVouaaar != undefined) {
          result.hasVideoVouaaar = args.stageInfos.hasVideoVouaaar;
        }
        if (args.stageInfos.extendStage != undefined) {
          result.extendStage = args.stageInfos.extendStage;
        }
        if (args.stageInfos.contactId) {
          result.contact_id = args.stageInfos.contactId;
        } else {
          result.contact_id = context.req?.session?.user?.id;
        }
        await result.save();


        if (args.openingHours) {
          await result.openingHours.forEach(async (openingHourdto) => {
            await openingHourdto.destroy();
          });
          var openingHourUpdated = [];
          await args.openingHours.forEach(async (openingHourdto) => {
            const openingHour = await OpeningHours.create({
              days: openingHourdto.days,
              hours: openingHourdto.hours,
              place: openingHourdto.place ? openingHourdto.place : '',
            });
            await openingHour.setStage(result.id);
            openingHourUpdated.push(openingHour);
          });

        }

        let entriesWithInformationAlreadeyCreated = [];
        if (args.stageInfos.entriesWithInformation) {
          entriesWithInformationAlreadeyCreated = args.stageInfos.entriesWithInformation.map(entryWithInformation => { return parseInt(entryWithInformation.entryId) })

          var existingEntries = [];
          result.entries.forEach(async (entry) => {
            existingEntries.push(entry.id);
          });


          args.stageInfos.entriesWithInformation.forEach(async (entryWithInformation) => {
            if (!existingEntries.includes(parseInt(entryWithInformation.entryId))) {
              await result.addEntry(entryWithInformation.entryId, { through: { topSEO: entryWithInformation.topSEO, linkDescription: (entryWithInformation.linkDescription != undefined ? entryWithInformation.linkDescription : null) } })
            } else {
              let resultAddEntries = result.entries.forEach(async (entry) => {
                if (entry.id === parseInt(entryWithInformation.entryId, 10)) {
                  entry.stageEntries.linkDescription = entryWithInformation.linkDescription;
                  entry.stageEntries.topSEO = entryWithInformation.topSEO;
                  await entry.stageEntries.save();
                }

              });
              if (resultAddEntries !== undefined) {
                Promise.all(resultAddEntries);
              }
            }

          });
          //     await Promise.all(resultEntriesWithInformation);

        }




        if (args.stageInfos.entries) {
          var existingEntries = [];
          result.entries.forEach(async (entry) => {
            if (parseInt(entry.id) > 0) {

              existingEntries.push(entry.id);

              if (!args.stageInfos.entries.includes('' + entry.id)) {
                await result.removeEntry(entry);
              }
            }
          });

          args.stageInfos.entries.forEach(async (entry) => {
            if (parseInt(entry) > 0) {
              if (!existingEntries.includes(parseInt(entry)) && !entriesWithInformationAlreadeyCreated.includes(parseInt(entry))) {
                await result.addEntry(entry);
              }
            }
          });
        }

        if (args.stageInfos.referents) {
          var existingReferents = [];
          result.referents.forEach(async (referent) => {
            existingReferents.push(referent.id);

            if (!args.stageInfos.referents.includes(referent.id.toString())) {
              await result.removeReferent(referent);
            }
          });

          args.stageInfos.referents.forEach(async (referentId) => {
            if (!existingReferents.includes(parseInt(referentId))) {
              await result.addReferent(referentId);
              const referent = await User.findByPk(referentId);
              const emailTitle = "Tu es référent⋅e de " + result.name + " sur le site du Tour Alternatiba 2024!";

              const emailTemplate = 'addReferentStage';

              const data = {
                nomEtape: result.name,
                idStage: result.id,
                nomReferent: referent.surname,
                urlStageArea: process.env.CLIENT_URL + "/admin/stages"
              };
              await sendEmail(referent.email + ',' + process.env.EMAIL_CONTACT, emailTitle, emailTemplate, data);
            }
          });
        }
        if (args.stageInfos.memberOf) {
          var existingMemberOf = [];
          result.memberOf.forEach(async (memberOf) => {
            existingMemberOf.push(memberOf.id);

            if (!args.stageInfos.memberOf.includes(memberOf.id.toString())) {
              await result.removeMemberOf(memberOf);
            }
          });

          args.stageInfos.memberOf.forEach(async (memberOfId) => {
            if (!existingMemberOf.includes(parseInt(memberOfId))) {
              await result.addMemberOf(memberOfId);
            }
          });
        }


        if (args.logoPictures) {
          const pictures = await managePicture(args.logoPictures, result.id);
        }

        if (args.mainPictures) {
          const pictures = await managePicture(args.mainPictures, result.id);
        }
        if (args.pictures) {
          const pictures = await managePicture(args.pictures, result.id);
        }

        if (args.partnerPictures) {
          const pictures = await managePicture(args.partnerPictures, result.id);
        }
        const stage = await Stage.findOne({
          where: { id: result.id },
          include: [
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },

                },

                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
              order: [{ model: StageEntries }, 'topSEO', 'ASC'],
            },
            {
              model: Event,
              as: 'events',
              attributes: ['id', 'label'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',
                    'position',
                    'logo',
                    'main',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
              ],
            },

            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: [],
              },
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
                'logo',
                'main',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
            {
              separate: true,
              model: OpeningHours,
              as: 'openingHours',
              attributes: ['id', 'days', 'hours', 'place'],
            },
          ],
        });
        stage.openingHour = openingHourUpdated;
        return stage;
      } catch (err) {
        console.log(err);
      }
    },

    addStageVolunteer: async (parent, args, context, infos) => {
      try {
        const stage = await Stage.findOne({
          where: { id: args.stageId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname', 'email'],
              through: {
                attributes: ['user_id', 'stage_id'],
              },
            }]
        });


        const stageVolunteer = await StageVolunteer.create({
          stageId: args.stageId,
          firstname: args.volunteerInfos.firstName,
          lastname: args.volunteerInfos.name,
          email: args.volunteerInfos.email,
          phone: args.volunteerInfos.phone,
          userNote: args.volunteerInfos.message,
        });

        // Email volunteer
        await sendEmail(args.volunteerInfos.email, `Contact étape ${stage.name} du Tour Alternatiba`, 'newVolunteer', {
          firstName: args.volunteerInfos.firstName,
          stageName: stage.name,
          stageLink: `${process.env.CLIENT_URL}/etape/${stage.name}`,
          stageEmail: stage.email,
        });

        // Email Referents
        await sendEmail(stage.email, `Volontaire étape ${stage.name} du Tour Alternatiba`, 'newVolunteerReferent', {
          firstName: args.volunteerInfos.firstName,
          lastName: args.volunteerInfos.name,
          email: args.volunteerInfos.email,
          phone: args.volunteerInfos.phone || '',
          message: args.volunteerInfos.message || '',
          stageName: stage.name,
          stageLink: `${process.env.CLIENT_URL}/etape/${stage.name}`,
        });

        return true;
      } catch (error) {
        console.log(error);
      }
    },

    deleteStage: async (parent, args, context, infos) => {
      try {
        const stage = await Stage.findOne({
          where: { id: args.stageId },
          include: [
            {
              model: User,
              as: 'referents',
              attributes: ['id', 'surname', 'lastname'],
              through: {
                attributes: ['user_id'],
              },
            },
            {
              model: Entry,
              as: 'entries',
              include: [
                {
                  model: Entry,
                  as: 'parentEntry',
                  attributes: ['id', 'label', 'position'],
                  include: {
                    model: Collection,
                    as: 'collection',
                    attributes: ['id', 'code', 'label', 'position'],
                  },

                },

                {
                  model: Collection,
                  as: 'collection',
                  attributes: ['id', 'code', 'label', 'position'],
                },
              ],
              order: [{ model: StageEntries }, 'topSEO', 'ASC'],
            },
            {
              model: Event,
              as: 'events',
              attributes: ['id', 'label'],
              through: {
                attributes: [],
              },
              include: [
                {
                  separate: true,
                  model: Picture,
                  as: 'pictures',
                  attributes: [
                    'id',
                    'label',
                    'originalPicturePath',
                    'originalPictureFilename',

                    'position',
                    'logo',
                    'main',
                  ],
                  orderBy: [
                    // ['activated','DESC'],
                    ['position', 'ASC'],
                  ],
                },
                {
                  model: User,
                  as: 'referents',
                  attributes: ['id', 'surname', 'lastname'],
                  through: {
                    attributes: ['user_id', 'event_id'],
                  },
                },
                {
                  model: Entry,
                  as: 'entries',
                  include: [
                    {
                      model: Entry,
                      as: 'parentEntry',
                      attributes: ['id', 'label', 'position'],
                      include: {
                        model: Collection,
                        as: 'collection',
                        attributes: ['id', 'code', 'label', 'position'],
                      },

                    },

                    {
                      model: Collection,
                      as: 'collection',
                      attributes: ['id', 'code', 'label', 'position'],
                    },
                  ],
                  order: [{ model: StageEntries }, 'topSEO', 'ASC'],
                },
              ],
            },
            {
              separate: true,
              model: Picture,
              as: 'pictures',
              attributes: [
                'id',
                'label',
                'originalPicturePath',
                'originalPictureFilename',
                'position',
              ],
              orderBy: [
                // ['activated','DESC'],
                ['position', 'ASC'],
              ],
            },
          ],
        });
        const referentsDeletion = stage.referents.map(async (referent) => {
          await stage.removeReferent(referent);
        });
        await Promise.all(referentsDeletion);
        const eventsDeletion = stage.events.map(async (event) => {
          if (args.deleteEvent && event.referents.length < 2) {

            const referentsDeletionEvent = event.referents.map(async (referent) => {
              await event.removeReferent(referent);
            });
            await Promise.all(referentsDeletionEvent);
            const entriesEventDeletion = event.entries.map(async (entry) => {
              await event.removeEntry(entry);
            });
            await Promise.all(entriesEventDeletion);

            const picturesEventDeletion = event.pictures.map(async (picture) => {
              await event.removePictures(picture);
            });
            await Promise.all(picturesEventDeletion);

            await event.removeStage(stage);

            await Event.destroy({
              where: {
                id: event.id,
              },
            });

          } else {
            await event.removeStage(stage);
          }

        });
        await Promise.all(eventsDeletion);
        const entriesStageDeletion = stage.entries.map(async (entry) => {
          await stage.removeEntry(entry);
        });
        await Promise.all(entriesStageDeletion);

        const picturesDeletion = stage.pictures.map(async (picture) => {
          await stage.removePictures(picture);
        });
        await Promise.all(picturesDeletion);
        // checker si le user est admin d'un stage qui est admin de l'stage
        const result = await Stage.destroy({
          where: {
            id: args.stageId,
          },
        });



        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
  Stage: {
    events: async (parent) => {
      const events = await Event.findAll({
        include: [
          {
            model: Stage,
            as: 'stages',
            attributes: ['id'],
            where: {
              id: parent.id,
            },
          }
        ]
      });

      return events;
    },
    volunteers: async (parent, args, context) => {
      if (isStageAdmin(parent, context)) {
        const volunteers = await StageVolunteer.findAll({
          where: { stageId: parent.id },
          order: [['createdAt', 'DESC']]
        })

        return volunteers;
      }

      return [];
    }
  }
};
const managePicture = async (pictures, stageId) => {
  //upload files
  if (pictures) {
    const uploadedFilesPromise = await Promise.all(
      pictures.map((image, index) => {
        return new Promise((resolve, reject) => {
          if (image.newpic && !image.deleted) {
            // uploader la nouvelle image sauf si deleted = true

            const fileToken = randToken.uid(10);
            Promise.all([
              moveNewPictureToStage(
                image.file.filename,
                `${stageId}`,
              ),
            ])
              .then((value) => {
                resolve({
                  // id : image.id,
                  newpic: image.newpic,
                  // activated : image.activated,
                  deleted: image.deleted,
                  logo: image.logo,
                  main: image.main,
                  partner: image.partner,
                  originalPicturePath: value[0].url,
                  originalPictureFilename: `${stageId}-${fileToken}-original`,
                  position: index,
                  stageId: stageId,
                });
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve({
              id: image.id,
              newpic: image.newpic,
              deleted: image.deleted,
              logo: image.logo,
              main: image.main,
              partner: image.partner,
              position: index,
              // productId:product_id
            });
          }
        });
      }),
    ).catch((err) => {
      console.log(err);
      // delete the product if an error occured
      // thow the error to the client
      throw new Error('File creation error');
    });

    const uploadedFiles = await uploadedFilesPromise;

    return uploadedFiles.map(async (file) => {
      if (!file.newpic && file.deleted) {
        // supprimer une image déja existante

        return new Promise((resolve, reject) => {
          Picture.findByPk(file.id).then((picture) => {
            fs.unlink(
              `/static/images/stage/${stageId}/${picture.originalPictureFilename}.jpeg`,
              () => {
                picture.destroy().then((value) => {
                  resolve({
                    id: file.id,
                    destroyed: value,
                  });
                });
              },
            );
          });
        });
      } else {
        // sinon traiter les images à updater

        if (file.newpic) {
          if (file.originalPictureFilename) {
            // si c'est une nouvelle image
            const result = Picture.create(file);
            // position = position +1;

            const picture = await result;
            picture.setStage(stageId);
            return result;
          } else {
            return null;
          }
        } else {
          // si l'image est deja existante, il faut updater sa position
          const result = Picture.update(
            file,
            // {
            // position : file.position
            // }
            { where: { id: file.id } },
          );
          // position = position +1;
          return result;
        }
      }
    });

    const result = await Promise.all(_);
    /*  result.map((picture) => {
        if(picture.dataValues){
        picture.dataValues.setStage(stageId)
        }
    });
*/

    //console.log(result);
  }
};

export default composeResolvers(stageResolvers, {
  'Mutation.createStage': [isAuthenticated(), isAdmin()],
  'Mutation.editStage': [isAuthenticated()],
  'Mutation.deleteStage': [isAuthenticated(), isAdmin()],
  'Mutation.addStageVolunteer': [],
});
