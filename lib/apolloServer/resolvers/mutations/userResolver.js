import { User, Stage } from 'lib/sequelize/models';
import { composeResolvers } from '@graphql-tools/resolvers-composition';
import randToken from 'rand-token';
import { GraphQLError } from 'graphql';
import { isAdmin, isAuthenticated } from '../middleware';
import sendValidationEmail from 'utils/email';

const userResolvers = {
  Query: {
    users: async (parent, args, context, info) => {
      const users = await User.findAll({
        include: [
          {
            model: Stage,
            as: 'adminstages',
          },
        ]
      });

      const formattedUsers = users.map(u => {
        const user = u.get({ plain: true});
        return { ...user, stages: user.adminstages };
      });

      return formattedUsers;
    },
    user: async (parent, args, context, info) => {
      const user = await User.findOne({
        where: { id: args.id },
        include: [
          {
            model: Stage,
            as: 'adminstages',
          },
        ]
      });

      const u = user.get({ plain: true});
      return { ...u, stages: u.adminstages };
    },
  },
  Mutation: {
    addUser: async(parent, args, context, info) => {
      try {
        const token = randToken.uid(20);

        const user = await User.create({
          surname: args.surname,
          lastname: args.lastname,
          email: args.email,
          role: args.role,
          isEmailValidated: false,
          validationEmailToken: token,
        });

        if (!user) {
          throw new GraphQLError( 'Utilisateur non créé');
        }

        await user.setAdminstages(args.stageIds);

        sendValidationEmail(user, token, (err, info) => {
          if (err) console.log(err);
        });

        return user;
      } catch (error) {
        console.log(error);
        throw new GraphQLError( 'Utilisateur non créé');
      }
    },

    sendValidationEmail: async(parent, args, context, info) => {
      try {
        const user = await User.findOne({
          where: { id: args.userId },
        });

        if (!user) {
          throw new GraphQLError('Utilisateur non trouvé');
        }

        if (user.isEmailValidated) {
          throw new GraphQLError('Utilisateur déjà validé');
        }

        const validationEmailToken = randToken.uid(20);
        await user.update({ validationEmailToken: validationEmailToken });
        sendValidationEmail(user, user.validationEmailToken, (err, info) => {
          if (err) console.log(err);
        });
        return true;

      } catch (error) {
        console.log(error);
        throw new GraphQLError("Erreur durant la mise à jour de l'utilisateur");
      }
    },

    editUser: async(parent, args, context, info) => {
      try {
        const user = await User.findOne({
          where: { id: args.userId },
          include: [
            {
              model: Stage,
              as: 'adminstages',
            },
          ]
        });

        if (!user) {
          throw new GraphQLError( 'Utilisateur non trouvé');
        }

        await User.update({
          surname: args.surname,
          lastname: args.lastname,
          email: args.email,
          role: args.role,
        }, {
          where: {
            id: args.userId,
          },
        });

        await user.setAdminstages(args.stageIds);
        await user.save();

        const updatedUser = await User.findOne({
          where: { id: args.userId },
          include: [
            {
              model: Stage,
              as: 'adminstages',
            },
          ]
        });

        const u = updatedUser.get({ plain: true});
        return { ...u, stages: u.adminstages };
      } catch (error) {
        console.log(error);
        throw new GraphQLError("Erreur durant la mise à jour de l'utilisateur");
      }
    },

    deleteUser: async (parent, args, context, infos) => {
      try {
        const result = await User.destroy({
          where: {
            id: args.userId,
          },
        });

        return result;
      } catch (err) {
        console.log(err);
      }
    },
  },
};

export default composeResolvers(userResolvers, {
  'Query.users': [isAuthenticated(), isAdmin()],
  'Query.user': [isAuthenticated(), isAdmin()],
  'Mutation.addUser': [isAuthenticated(), isAdmin()],
  'Mutation.sendValidationEmail': [isAuthenticated(), isAdmin()],
  'Mutation.editUser': [isAuthenticated(), isAdmin()],
  'Mutation.updateUserInfos': [isAuthenticated()],
  'Mutation.deleteUser': [isAuthenticated(), isAdmin()],
});
