import { toUpperCase } from "lib/handlebars/helpers";
import { sendEmail } from "lib/nodemailer/sendEmail";
import { InviteStage } from "lib/sequelize/models";
import fs from 'fs';
import {
  Stage, User
} from 'lib/sequelize/models';


const sendContactFormEmailResolver = {
  Mutation: {
    sendContactFormEmail: async (parentValue, args, context) => {
      const helpers = {
        uppercase: toUpperCase
      };
      const attachments = [];

      try {
        if (args.contactForm?.pictures) {
          let i = 0;
          for (const picture of args.contactForm.pictures) {
            const stream = fs.createReadStream(`./public/static/images/newUpload/${picture.file.filename}`);
            attachments.push({
              filename: `attachment-${i}.jpeg`,
              content: stream
            });
            fs.rm(`./public/static/images/newUpload/` + picture.file.filename, function (err) {
              if (err) throw err
              console.log('Successfully delete file' );
            })

            i++;
          }
        }
      } catch (error) {
        console.error(error);
      }
      sendEmail(
        args.contactForm.category?.toUpperCase()=="MESSAGE"?"tour@alternatiba.eu":"webmaster@alternatiba.eu",
        args.contactForm.object,
        'contactForm',
        args.contactForm,
        helpers,
        attachments
      );
      return true;
    },
    suggestEvent: async (parentValue, args, context) => {
      const helpers = {
        uppercase: toUpperCase
      };

      const stage = await Stage.findOne({ where: { id: args.suggestEventInfos.stageId } ,
        include: [
          {
            model: User,
            as: 'referents',
            attributes: ['id', 'surname', 'lastname','email'],
            through: {
              attributes: ['user_id', 'stage_id'],
            },
          }]});

      var emailReferents = stage.email+',' ;
      stage.referents.map((referent) => {
        emailReferents += referent.email+',';
      });
      args.suggestEventInfos.hasMessage=args.suggestEventInfos.message?true:false;
      args.suggestEventInfos.requesterName=args.suggestEventInfos.firstName?(args.suggestEventInfos.firstName+ ' '+args.suggestEventInfos.lastName):args.suggestEventInfos.requesterName;
      args.suggestEventInfos.hasRequester=args.suggestEventInfos.requesterName?true:false;
      args.suggestEventInfos.stageName=stage.name;
        sendEmail(
          emailReferents,
          (args.suggestEventInfos.contactName?`${args.suggestEventInfos.contactName}, `:``)+(args.suggestEventInfos.requesterName?`${args.suggestEventInfos.requesterName}`:`une personne`)+` vous propose d'ajouter votre événement sur OUAAA!`,
          'suggestEvent',
          args.suggestEventInfos,
          helpers,
        );
      return true;
    }

  },
}




export default sendContactFormEmailResolver;
