import authResolvers from 'lib/apolloServer/resolvers/mutations/authResolvers';
import userResolvers from 'lib/apolloServer/resolvers/mutations/userResolver';
import stageResolvers from 'lib/apolloServer/resolvers/mutations/stageResolver';
import eventResolvers from 'lib/apolloServer/resolvers/mutations/eventResolvers';
import newsletterResolvers from 'lib/apolloServer/resolvers/mutations/newsletterResolvers';
import collectionResolvers from 'lib/apolloServer/resolvers/mutations/collectionResolvers';
import contactResolver from 'lib/apolloServer/resolvers/mutations/contactResolver';
import searchResolvers from 'lib/apolloServer/resolvers/mutations/searchResolvers';
import favoriteResolvers from 'lib/apolloServer/resolvers/mutations/favoriteResolvers';
import articleResolvers from 'lib/apolloServer/resolvers/mutations/articleResolvers';

export default [
  authResolvers,
  userResolvers,
  stageResolvers,
  eventResolvers,
  newsletterResolvers,
  collectionResolvers,
  contactResolver,
  searchResolvers,
  favoriteResolvers,
  articleResolvers,
];
