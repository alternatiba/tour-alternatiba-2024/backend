import { sendEmail } from 'lib/nodemailer/sendEmail';

const emailTitle = 'Renouvellement du mot de passe';

const emailTemplate = 'resetPasswordCodeEmail';

const sendResetPasswordEmail = async (to: string,surname: string, code: string) => {
  const data = {
    code,surname
  };

  return await sendEmail(to, emailTitle, emailTemplate, data);
};

export default sendResetPasswordEmail;
