import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from 'lib/sequelize/db';

const User = sequelize.define(
  'User',
  {
    surname: {
      type: DataTypes.STRING,
    },
    lastname: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
    },
    role: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    resetPasswordCode: {
      type: DataTypes.INTEGER(4),
    },
    resetPasswordCodeDate: {
      type: DataTypes.DATE,
    },
    isEmailValidated: {
      type: DataTypes.BOOLEAN,
    },
    validationEmailToken: {
      type: DataTypes.STRING,
    },
    participatedAt: {
      type: DataTypes.VIRTUAL
    },
  },
  {
    tableName: 'user',
  },
);

const EventParticipant = sequelize.define(
  'EventParticipant',
  {
    firstname: {
      type: DataTypes.STRING,
    },
    lastname: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
    phone: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE
    },
    territory: {
      type: DataTypes.STRING,
    },
    association: {
      type: DataTypes.BOOLEAN,
    },
    associationName: {
      type: DataTypes.STRING,
    },
    experience: {
      type: DataTypes.STRING,
    },
    token: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: 'event_participant',
  },
);

const Stage = sequelize.define(
  'Stage',
  {
    name: {
      type: DataTypes.STRING,
    },
    startedAt: {
      type: DataTypes.DATE,
    },
    endedAt: {
      type: DataTypes.DATE,
    },
    email: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    website: {
      type: DataTypes.STRING,
    },
    socialNetwork: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.FLOAT,
    },
    lng: {
      type: DataTypes.FLOAT,
    },
    shortDescription: {
      type: DataTypes.STRING,
    },
    isValidated: {
      type: DataTypes.BOOLEAN,
    },
    dateValidation: {
      type: DataTypes.DATE,
    },
    activity: {
      type: DataTypes.STRING,
    },
    volunteerDescription: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.STRING,
    },
    nbVolunteers: {
      type: DataTypes.VIRTUAL
    },
    gameParticipationDate: {
      type: DataTypes.VIRTUAL
    },
    hasVideoVouaaar: {
      type: DataTypes.BOOLEAN,
    },
    extendStage: {
      type: DataTypes.BOOLEAN,
    },
    url: {
      type: DataTypes.STRING,
    },
    volunteerForm: {
      type: DataTypes.STRING,
    },
    volunteerAction: {
      type: DataTypes.ENUM('email', 'externForm', 'internForm'),
    },
  },
  {
    tableName: 'stage',
  },
);

const Event = sequelize.define(
  'Event',
  {
    label: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.TEXT,
    },
    facebookUrl: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    startedAt: {
      type: DataTypes.DATE,
    },
    endedAt: {
      type: DataTypes.DATE,
    },
    published: {
      type: DataTypes.BOOLEAN,
    },
    address: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    city: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.FLOAT,
    },
    lng: {
      type: DataTypes.FLOAT,
    },
    registerLink: {
      type: DataTypes.STRING,
    },
    practicalInfo: {
      type: DataTypes.STRING,
    },
    nbParticipants: {
      type: DataTypes.VIRTUAL
    },
    dateRule: {
      type: DataTypes.STRING,
    },
    limitPlace: {
      type: DataTypes.INTEGER,
    },
    url: {
      type: DataTypes.STRING,
    },
    category: {
      type: DataTypes.STRING,
    },
    participateButton: {
      type: DataTypes.BOOLEAN,
    }
  },
  {
    tableName: 'event',
  },
);

const Picture = sequelize.define(
  'picture',
  {
    label: {
      type: DataTypes.STRING,
      // unique: true,
    },
    originalPicturePath: {
      type: DataTypes.STRING,
      unique: true,
    },
    originalPictureFilename: {
      type: DataTypes.STRING,
      unique: true,
    },
    position: {
      type: DataTypes.INTEGER,
    },
    logo: {
      type: DataTypes.BOOLEAN,
    },
    main: {
      type: DataTypes.BOOLEAN,
    },
    partner: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: 'picture',
  },
);

const Newsletter = sequelize.define(
  'Newsletter',
  {
    email: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: 'newsletter',
  },
);

const StageEntries = sequelize.define(
  'stageEntries',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    linkDescription: DataTypes.STRING,
    topSEO: DataTypes.BOOLEAN
  }
);

const Entry = sequelize.define(
  'Entry',
  {
    code: DataTypes.STRING,
    label: DataTypes.STRING,
    value: DataTypes.STRING,
    icon: DataTypes.STRING,
    color: DataTypes.STRING,
    description: DataTypes.STRING,
    position: DataTypes.INTEGER,
    enabled: DataTypes.BOOLEAN,
  },
  {
    tableName: 'entries',
  },
);

const Collection = sequelize.define(
  'Collection',
  {
    code: DataTypes.STRING,
    label: DataTypes.STRING,
    multipleSelection: DataTypes.BOOLEAN,
    withDescription: DataTypes.BOOLEAN,
    value: DataTypes.STRING,
    position: DataTypes.INTEGER,
    enabled: DataTypes.BOOLEAN,
    stage: DataTypes.BOOLEAN,
    event: DataTypes.BOOLEAN,
    filter: DataTypes.BOOLEAN,
  },
  {
    tableName: 'collections',
  },
);

const OpeningHours = sequelize.define(
  'OpeningHour',
  {
    days: DataTypes.JSON,
    hours: DataTypes.JSON,
    place: DataTypes.JSON,
  },
  {
    tableName: 'opening_hours',
  },
);

const CodeControls = sequelize.define(
  'code_controls',
  {
    code: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.ENUM(['open', 'cancel', 'close']),
      allowNull: false,
    },
    action: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    info1: {
      type: DataTypes.STRING,
    },
    info2: {
      type: DataTypes.STRING,
    },
  }
);

const InviteStage = sequelize.define(
  'InviteStage',
  {
    requesterName: {
      type: DataTypes.STRING,
    },
    contactName: {
      type: DataTypes.STRING,
    },
    stageName: {
      type: DataTypes.STRING,
    },
    stageEmail: {
      type: DataTypes.STRING,
    },
    postCode: {
      type: DataTypes.STRING,
    },
    sendEmail: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: 'inviteStage',
  },
);

const StageVolunteer = sequelize.define(
  'StageVolunteer',
  {
    firstname: {
      type: DataTypes.STRING,
    },
    lastname: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
    phone: {
      type: DataTypes.STRING,
    },
    userNote: {
      type: DataTypes.STRING,
    },
    adminNote: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE
    },
  },
  {
    tableName: 'stage_volunteer',
  },
);

CodeControls.createCodeInstance = async (
  action,
  info1 = null,
  info2 = null,
) => {
  return await CodeControls.create({
    code: Math.floor(1000 + Math.random() * 9000),
    status: 'open',
    action,
    info1,
    info2,
  });
};

const Article = sequelize.define(
  'Article',
  {
    label: {
      type: DataTypes.STRING,
    },
    shortDescription: {
      type: DataTypes.TEXT,
    },
    content: {
      type: DataTypes.TEXT('long'),
    },
    url: {
      type: DataTypes.STRING,
    },
    bannerPrincipalPicture: {
      type: DataTypes.BOOLEAN,
    },
  },
  {
    tableName: 'article',
  },
);

const startDB = () => {
  sequelize.sync();

  Event.belongsToMany(User, {
    through: 'user_event_participant',
    as: 'participants',
    foreignKey: 'event_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_participant',
    as: 'events',
    foreignKey: 'user_id',
  });

  Stage.belongsToMany(User, {
    through: 'user_stage_game',
    as: 'gamers',
    foreignKey: 'stage_id',
  });
  Stage.belongsToMany(User, {
    through: 'user_stage_favorite',
    as: 'favorites',
    foreignKey: 'stage_id',
  });
  Stage.belongsToMany(Entry, {
    through: StageEntries,
    as: 'entries',
    foreignKey: 'stage_id',
  });

  Entry.belongsToMany(Stage, {
    through: StageEntries,
    as: 'stages',
    foreignKey: 'entry_id',
  });


  Event.belongsToMany(Entry, {
    through: 'event_entry',
    as: 'entries',
    foreignKey: 'event_id',
  });
  Event.belongsToMany(User, {
    through: 'user_event_favorite',
    as: 'favorites',
    foreignKey: 'event_id',
  });
  Entry.belongsToMany(Event, {
    through: 'event_entry',
    as: 'events',
    foreignKey: 'entry_id',
  });
  Entry.hasMany(Entry, { as: 'subEntries', foreignKey: 'entry_id' });
  Entry.belongsTo(Entry, { as: 'parentEntry', foreignKey: 'entry_id' });

  Event.hasMany(Event, { as: 'subEvents', foreignKey: 'event_id' });
  Event.belongsTo(Event, { as: 'parentEvent', foreignKey: 'event_id' });

  Entry.belongsTo(Collection, {
    as: 'collection',
    foreignKey: 'collection_id',
  });
  Collection.hasMany(Entry, { as: 'entries', foreignKey: 'collection_id' });

  User.belongsToMany(Stage, {
    through: 'user_stage_volunteer',
    as: 'stages',
    foreignKey: 'user_id',
  });

  Stage.belongsToMany(User, {
    through: 'user_stage_game',
    as: 'gameStages',
    foreignKey: 'stage_id',
  });
  User.belongsToMany(Stage, {
    through: 'user_stage_favorite',
    as: 'favoriteStages',
    foreignKey: 'user_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_favorite',
    as: 'favoriteEvents',
    foreignKey: 'user_id',
  });
  User.belongsToMany(Stage, {
    through: 'user_stage_admin',
    as: 'adminstages',
    foreignKey: 'user_id',
  });


  Stage.belongsToMany(Event, {
    through: 'stage_event',
    as: 'events',
    foreignKey: 'stage_id',
  });
  Event.belongsToMany(Stage, {
    through: 'stage_event',
    as: 'stages',
    foreignKey: 'event_id',
  });

  Event.belongsToMany(User, {
    through: 'user_event_admin',
    as: 'referents',
    foreignKey: 'event_id',
  });
  Stage.belongsToMany(User, {
    through: 'user_stage_admin',
    as: 'referents',
    foreignKey: 'stage_id',
  });
  User.belongsToMany(Event, {
    through: 'user_event_admin',
    as: 'adminevents',
    foreignKey: 'user_id',
  });

  Stage.belongsToMany(Article, {
    through: 'stage_article',
    as: 'articles',
    foreignKey: 'stage_id',
  });
  Article.belongsToMany(Stage, {
    through: 'stage_article',
    as: 'stages',
    foreignKey: 'article_id',
  });

  Newsletter.belongsTo(User, { foreignKey: 'user_id' });
  User.hasOne(Newsletter, { foreignKey: 'user_id' });

  Newsletter.belongsTo(Event, { foreignKey: 'event_id' });
  Event.hasOne(Newsletter, { foreignKey: 'event_id' });

  Newsletter.belongsTo(Stage, { foreignKey: 'stage_id' });
  Stage.hasOne(Newsletter, { foreignKey: 'stage_id' });

  Picture.belongsTo(Stage, { foreignKey: 'stage_id' });
  Stage.hasMany(Picture, { foreignKey: 'stage_id', as: 'pictures' });

  Picture.belongsTo(Event, { foreignKey: 'event_id' });
  Event.hasMany(Picture, { foreignKey: 'event_id', as: 'pictures' });

  Picture.belongsTo(Article, { foreignKey: 'article_id' });
  Article.hasMany(Picture, { foreignKey: 'article_id', as: 'pictures' });

  Stage.belongsTo(User, {
    foreignKey: 'validated_stage_id',
    as: 'userValidated',
  });
  User.hasMany(Stage, { foreignKey: 'validated_stage_id', as: 'validatedStages' });

  Stage.belongsTo(User, {
    foreignKey: 'contact_id',
    as: 'userContact',
  });
  User.hasMany(Stage, { foreignKey: 'contact_id', as: 'contactStages' });

  OpeningHours.belongsTo(Stage, {
    as: 'stage',
    foreignKey: 'stageId',
  });
  Stage.hasMany(OpeningHours, { as: 'openingHours', foreignKey: 'stageId' });

  Stage.belongsToMany(Stage, {
    through: 'stage_memberof_stage',
    as: 'members',
    foreignKey: 'stage_id',
  });

  Stage.belongsToMany(Stage, {
    through: 'stage_memberof_stage',
    as: 'memberOf',
    foreignKey: 'stagememberof_id',
  });

  Stage.hasMany(Stage, { as: 'proposedStage', foreignKey: 'referencing_stage_id' });
  Stage.belongsTo(Stage, { as: 'referencingStage', foreignKey: 'referencing_stage_id' });

  Stage.hasMany(StageVolunteer, { as: 'volunteers', foreignKey: 'stageId' });
  StageVolunteer.belongsTo(Stage, { as: 'stage', foreignKey: 'stageId' });

  Event.hasMany(EventParticipant, { as: 'participantEvents', foreignKey: 'eventId' });
  EventParticipant.belongsTo(Event, { as: 'event', foreignKey: 'eventId' });
};

export {
  User,
  CodeControls,
  Stage,
  Event,
  Newsletter,
  Picture,
  Entry,
  Collection,
  StageEntries,
  OpeningHours,
  InviteStage,
  Article,
  EventParticipant,
  StageVolunteer,
};

export default startDB;
