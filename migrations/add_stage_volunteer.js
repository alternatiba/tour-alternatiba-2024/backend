module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('stage_volunteer', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            firstname: {
                type: Sequelize.STRING
            },
            lastname: {
                type: Sequelize.STRING
            },
            email: {
                type: Sequelize.STRING,
            },
            phone: {
                type: Sequelize.STRING
            },
            userNote: {
                type: Sequelize.STRING
            },
            adminNote: {
                type: Sequelize.STRING
            },
            stageId: { // Foreign key column
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'stage', // Name of the referenced table
                    key: 'id' // Primary key of the referenced table
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('stage_volunteer');
    }
};
