module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('event', 'category', {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: 'other'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('event', 'category');
  },
};
