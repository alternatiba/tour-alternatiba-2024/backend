module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('stage', 'volunteerForm', {
            type: Sequelize.STRING,
            allowNull: true,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('stage','volunteerForm');
    },
};
