module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('event', 'participateButton', {
            type: Sequelize.BOOLEAN,
            allowNull: true,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('event','participateButton');
    },
};
