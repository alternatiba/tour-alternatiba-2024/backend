module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('stage', 'volunteerAction', {
            type: Sequelize.ENUM,
            values: ['email', 'externForm', 'internForm'],
            allowNull: false,
            defaultValue: 'email',
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('stage','volunteerAction');
    },
};
