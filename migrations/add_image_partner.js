module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn('picture', 'partner', {
            type: Sequelize.BOOLEAN,
            allowNull: true,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('picture','partner');
    },
};
