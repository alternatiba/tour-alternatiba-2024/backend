module.exports = {
    up: async (queryInterface, Sequelize) => {


        // Add columns to the event_participant table
        await queryInterface.addColumn('event_participant', 'token', {
            type: Sequelize.STRING
        });
    },
    down: async (queryInterface, Sequelize) => {
        // Remove columns from the event_participant table
        await queryInterface.removeColumn('event_participant', 'token');

    
    }
};