module.exports = {
    up: async (queryInterface, Sequelize) => {
        // Add uniqueness constraint on eventId and email
        await queryInterface.addConstraint('event_participant', {
            fields: ['eventId', 'email'],
            type: 'unique',
            name: 'unique_event_participant_eventId_email'
        });
    },
    down: async (queryInterface, Sequelize) => {
        // Remove uniqueness constraint before dropping the table
        await queryInterface.removeConstraint('event_participant', 'unique_event_participant_eventId_email');
    }
};