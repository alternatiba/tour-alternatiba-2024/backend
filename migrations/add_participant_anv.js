module.exports = {
    up: async (queryInterface, Sequelize) => {


        // Add columns to the event_participant table
        await queryInterface.addColumn('event_participant', 'territory', {
            type: Sequelize.STRING
        });

        await queryInterface.addColumn('event_participant', 'association', {
            type: Sequelize.BOOLEAN
        });

        await queryInterface.addColumn('event_participant', 'associationName', {
            type: Sequelize.STRING
        });

        await queryInterface.addColumn('event_participant', 'experience', {
            type: Sequelize.STRING
        });
    },
    down: async (queryInterface, Sequelize) => {
        // Remove columns from the event_participant table
        await queryInterface.removeColumn('event_participant', 'territory');
        await queryInterface.removeColumn('event_participant', 'association');
        await queryInterface.removeColumn('event_participant', 'associationName');
        await queryInterface.removeColumn('event_participant', 'experience');

    
    }
};