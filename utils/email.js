
import nodemailer from 'nodemailer';

var mailerhbs = require('nodemailer-express-handlebars');

const sendValidationEmail = async (user, token, callback = null) => {
  var emailTransporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    secureConnection: process.env.EMAIL_SECURECONECTION === 'true' ? true : false,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false,
    },
  });

  var urlValidation =
  process.env.CLIENT_URL + '/accountValidation/' + user.email + '/' + token;
  const mailOptions = {
    from: {
      name: process.env.EMAIL_FROM,
      address: process.env.EMAIL_USER,
    }, // sender address
    to: user.email, // list of receivers
    subject: 'Ton compte sur le site du Tour Alternatiba',
    template: 'validationEmail', //Name email file template
    context: {
      // pass variables to template
      surname: user.surname,
      lastname: user.lastname,
      email: user.email,
      urlValidation: urlValidation,
    },
  };
  emailTransporter.use(
    'compile',
    mailerhbs({
      viewEngine: {
        extName: '.hbs',
        partialsDir: './emailTemplates',
        layoutsDir: './emailTemplates',
        defaultLayout: 'validationEmail.hbs',
      },
      viewPath: './emailTemplates', //Path to email template folder
      extName: '.hbs', //extension of email template
    }),
  );

  //let info = await emailTransporter.sendMail(mailOptions)
  if (callback) emailTransporter.sendMail(mailOptions, callback);
  else emailTransporter.sendMail(mailOptions);
};

export default sendValidationEmail;
