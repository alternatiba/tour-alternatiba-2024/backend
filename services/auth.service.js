import randToken from 'rand-token';

const issueRefreshToken = () => {
  return randToken.uid(256);
};

const issueEmailValidationToken = () => {
  return randToken.uid(20);
};

export {
  issueRefreshToken,
  issueEmailValidationToken,
};
